#!/bin/sh

LEAVING=0

GUNICORN_PID=0
LAVA_LOGS_PID=0
LAVA_MASTER_PID=0
LAVA_PUBLISHER_PID=0

# Trap signals
trap "LEAVING=1" HUP INT QUIT TERM

# Start all services
echo "Starting postgresql"
/etc/init.d/postgresql start
echo "done"
echo

echo "Starting gunicorn"
gunicorn3 lava_server.wsgi&
GUNICORN_PID=$!
echo "done"
echo

echo "Starting apache2"
/etc/init.d/apache2 start
echo "done"
echo

echo "Starting lava-logs"
lava-server manage lava-logs&
LAVA_LOGS_PID=$!
echo "done"
echo

echo "Starting lava-publisher"
lava-server manage lava-publisher&
LAVA_PUBLISHER_PID=$!
echo "done"
echo

echo "Starting lava-master"
lava-server manage lava-master&
LAVA_MASTER_PID=$!
echo "done"
echo

# Create the user and token
lava-server manage shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')"
lava-server manage tokens add --user admin --secret admin

echo "Waiting for a signal"
while [ $LEAVING = 0 ]
do
  sleep 5
done

# Stopping services
if [ $LAVA_LOGS_PID != 0 ]
then
  echo "stopping lava-logs"
  kill $LAVA_LOGS_PID
  wait $LAVA_LOGS_PID
fi

if [ $LAVA_MASTER_PID != 0 ]
then
  echo "stopping lava-master"
  kill $LAVA_MASTER_PID
  wait $LAVA_MASTER_PID
fi

if [ $LAVA_PUBLISHER_PID != 0 ]
then
  echo "stopping lava-publisher"
  kill $LAVA_PUBLISHER_PID
  wait $LAVA_PUBLISHER_PID
fi

if [ $GUNICORN_PID != 0 ]
then
  echo "stopping gunicorn"
  kill $GUNICORN_PID
  wait $GUNICORN_PID
fi

echo "exited $0"
