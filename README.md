lava-play
=========

**lava-play** is a small script to quickly setup and play with a small LAVA instance.

Keep in mind that this instance should only be used to play with LAVA and not
for any production purpose.

Using lava-play
===============

Requirements
------------

lava-play is running only under Python3.

It depends on (see **requirements.txt**):

* docker
* requests


Installing
----------

lava-play can only be executed from sources:

    git clone https://git.lavasoftware.org/lava/lava-play.git
    cd lava-play/
    python3 lava-play


Using it
--------

lava-play will:

* build lava-server and lava-dispatcher docker images
* create a specific docker network
* run lava-server and lava-dispatcher containers
* create a qemu device
* submit a basic qemu job
